<?php

namespace Drupal\kits;

use Drupal\kits\Services\KitsInterface;

/**
 * Interface KitInterface
 *
 * @package Drupal\kits
 */
interface KitInterface
{

    public static function create(KitsInterface $kitsService, string $id = NULL, array $parameters = [], array $context = []): static;

    public function append(KitInterface $kit): static;

    public function appendParent(string $parentID): static;

    /**
     * Exclude a particular parameter when building the render array.
     */
    public function excludeParameter(string $parameter): static;

    public function get(string $key, array|string|null $default = NULL): array|string|null;

    public function getArray(): array;

    public function getID(): string;

    public function getChildrenArray(): array;

    public function getContext(string $key, array|string|null $default = NULL): array|string|null;

    public function getParents(): array;

    public function has(string $key): bool;

    public function set(string $key, array|string|null $value): static;

    public function setContext(string $key, array|string|null $value): static;

    public function setID(string $value): static;

    public function setGroup(string $group): static;
}
