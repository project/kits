<?php

namespace Drupal\kits;

use Drupal\kits\Services\KitsInterface;

/**
 * Class Kit
 *
 * @package Drupal\formfactorykits\Kits
 */
abstract class Kit implements KitInterface {
    public const ID_KEY = 'id';
    const GROUP_KEY = 'group';

    public static ?string $id = NULL;

    protected array $context;

    protected array $excludedParameters = [];

    protected array $keys;

    /**
     * @var Kit[]
     *   Any child kits associated with this kit.
     */
    protected array $kits = [];

    protected KitsInterface $kitsService;

    protected array $parameters;

    protected bool $includeParentsInChildrenArray = TRUE;

    public function __construct(KitsInterface $kitsService,
                                ?string       $id = NULL,
                                array         $parameters = [],
                                array         $context = [])
    {
        $this->kitsService = $kitsService;
        if (NULL !== $id) {
            $context[self::ID_KEY] = $id;
        }
        if (!array_key_exists(self::ID_KEY, $context) && NULL !== static::$id) {
            $context[self::ID_KEY] = static::$id;
        }
        $this->parameters = $parameters;
        $this->context = $context;
    }

    public static function create(KitsInterface $kitsService, string $id = NULL, array $parameters = [], array $context = []): static
    {
        return new static($kitsService, $id, $parameters, $context);
    }

    public function append(KitInterface $kit): static
    {
        $this->kits[] = $kit;
        return $this;
    }

    public function appendParent(string $parentID): static
    {
        $this->context['parents'][] = $parentID;
        return $this;
    }

    public function excludeParameter($parameter): static
    {
        $this->excludedParameters[] = $parameter;
        return $this;
    }

    public function get(string $key, array|string|null $default = NULL): array|string|null
    {
        if (array_key_exists($key, $this->parameters)) {
            return $this->parameters[$key];
        } else {
            return $default;
        }
    }

    public function getChildrenArray(): array
    {
        $artifact = [];
        foreach ($this->kits as $kit) {
            /** @var Kit $kit */
            $id = $this->getID();
            if ($this->includeParentsInChildrenArray) {
                $kit->appendParent($id);
            }
            $kit->setGroup($id);
            $artifact[$kit->getID()] = $kit->getArray();
        }
        return $artifact;
    }

    public function getContext(string $key, array|string|null $default = NULL): array|string|null
    {
        if (array_key_exists($key, $this->context)) {
            return $this->context[$key];
        } else {
            return $default;
        }
    }

    public function getID(): string
    {
        $id = $this->getContext(self::ID_KEY);
        if (empty($id)) {
            throw new \LogicException(vsprintf('Could not find kit ID for %s', [
                static::class,
            ]));
        }
        return $id;
    }

    public function getParents(): array
    {
        if (array_key_exists('parents', $this->context)) {
            return $this->context['parents'];
        } else {
            return [];
        }
    }

    public function has($key): bool
    {
        return array_key_exists($key, $this->parameters);
    }

    public function set($key, $value): static
    {
        $this->parameters[$key] = $value;
        return $this;
    }

    public function setContext($key, $value): static
    {
        $this->context[$key] = $value;
        return $this;
    }

    public function setID($value): static
    {
        return $this->setContext(self::ID_KEY, $value);
    }

    public function setGroup($group): static
    {
        return $this->set(self::GROUP_KEY, $group);
    }
}
