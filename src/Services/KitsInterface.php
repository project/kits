<?php

namespace Drupal\kits\Services;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interface KitsInterface
 *
 * @package Drupal\kits\Services
 */
interface KitsInterface
{
    /**
     * Returns the currently active global container.
     */
    public function getContainer(): ?ContainerInterface;

    public function t(string $string, array $args = []): TranslatableMarkup;
}
