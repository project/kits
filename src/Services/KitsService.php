<?php

namespace Drupal\kits\Services;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class KitsService
 *
 * @package Drupal\kits\Services
 */
class KitsService implements KitsInterface
{

    private ContainerInterface $container;

    /**
     * KitsService constructor.
     */
    public function __construct()
    {
        // TODO: inject this, if possible
        $this->container = \Drupal::getContainer();
    }

    public function getContainer(): ?ContainerInterface
    {
        return $this->container;
    }

    private function getTranslationService(): TranslationInterface
    {
        /** @var TranslationInterface $service */
        static $service;
        if (NULL === $service) {
            $service = $this->container->get('string_translation');
        }
        return $service;
    }

    public function t(string $string, array $args = []): TranslatableMarkup
    {
        return $this->getTranslationService()->translate($string, $args);
    }
}
