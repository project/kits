# Kits

Provides a service capable of creating `Kit` objects.

These objects are able to generate arrays compatible with Drupal's Render API.

Presently `kits` merely serves as a foundation for `formfactorykits` module.
Please refer to the README in that module for more...

## Road Map
* implement unit test coverage:
  * confirm methods in KitsInterface return the expected results
  * confirm methods in mock class implementing KitInterface return expected results
